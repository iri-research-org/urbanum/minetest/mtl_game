#!/bin/bash

# Colors with 0 alpha need to be preserved, because opaque leaves ignore alpha.
# For that purpose, the use of indexed colors is disabled (-nc).

find .. -name '*.png' -print0 | xargs -0 optipng -o7 -zm1-9 -nc -strip all -clobber

# Set colorspace to sRGB to avoid color shifts in the game.
find .. -type f -exec file {} \; | grep -o -P '^.+: \w+ image'| cut -d":" -f1 | xargs -I {} sh -c 'magick mogrify -colorspace sRGB {}' 