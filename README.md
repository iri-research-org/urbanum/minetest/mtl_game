# UNEJMTL Minetest Game

The game used in MinetestLab.  

## Installation

- Unzip the archive, rename the folder to `mtl` and
place it in .. minetest/games/

- GNU/Linux: If you use a system-wide installation place
    it in ~/.minetest/games/.

The Minetest engine can be found at [GitHub](https://github.com/minetest/minetest).

For further information or help, see:  
https://wiki.minetest.net/Installing_Mods

## Licensing

See `LICENSE.txt`

## List of mods

| NOM                    | SOURCE                                                                             | FORK                                                                   |
|------------------------|------------------------------------------------------------------------------------|------------------------------------------------------------------------|
| areas                  | https://github.com/minetest-mods/areas |    |
| areas                  | https://github.com/minetest-game-mods/butterflies |    |
| default                | https://github.com/minetest-game-mods/default |    |
| dye                    | https://github.com/minetest-game-mods/dye |    |
| env_sounds             | https://github.com/minetest-game-mods/env_sounds |    |
| farming                | https://github.com/minetest-game-mods/farming |    |
| flowers                | https://github.com/minetest-game-mods/flowers |    |
| give_initial_stuff     | https://github.com/minetest-game-mods/give_initial_stuff |    |
| hardenedclay           | https://forge.apps.education.fr/iri/minetest/minetest-mod-hardenedclay |    |
| server_news            | https://github.com/Ezhh/server_news |    |
| mapserver_mod          | https://github.com/minetest-mapserver/mapserver_mod |    |
| maptools               | https://github.com/minetest-mods/maptools |    |
| meshport_ext           | https://forge.apps.education.fr/iri/minetest/meshport-ext.git |    |
| monitoring             | https://github.com/minetest-monitoring/monitoring |    |
| playerfactions         | https://github.com/mt-mods/playerfactions  |    |
| playerfactions_ext     | https://forge.apps.education.fr/iri/minetest/playerfactions_ext  |    |
| player_api             | https://github.com/minetest-game-mods/player_api |    |
| sethome                | https://github.com/minetest-game-mods/sethome |    |
| sfinv                  | https://github.com/minetest-game-mods/sfinv |    |
| stairs                 | https://github.com/minetest-game-mods/stairs |    |
| qos                    | https://github.com/S-S-X/qos |    |
| we_undo                | https://github.com/HybridDog/we_undo |    |
| whitelist              | https://github.com/AntumMT/mod-whitelist.git |    |
| worldedit              | https://github.com/Uberi/Minetest-WorldEdit |    |
| worldedit_bigschems    | https://forge.apps.education.fr/iri/minetest/worldedit_bigschems |    |

qos: for mapserver and meshport