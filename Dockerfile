ARG PUID="30000"
ARG GUID="30000"
ARG BUILD_DATE
ARG VERSION
ARG LUANTI_RELEASE
ARG MINETEST_GAME_RELEASE
ARG LUANTI_RELEASE_DEFAULT="5.10.0"
ARG MINETEST_GAME_RELEASE_DEFAULT="5.8.0"
ARG DOCKER_IMAGE=alpine:3.21

FROM $DOCKER_IMAGE AS dev
LABEL build_version="iri-research.org version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="I.R.I."

ENV LUAJIT_VERSION v2.1

RUN apk add --no-cache git build-base cmake curl-dev zlib-dev zstd-dev \
	sqlite-dev postgresql-dev hiredis-dev leveldb-dev file grep imagemagick \
	gmp-dev jsoncpp-dev ninja ca-certificates optipng bash curl

WORKDIR /usr/src/

RUN git clone --recursive https://github.com/jupp0r/prometheus-cpp && \
	cd prometheus-cpp && \
	cmake -B build \
	-DCMAKE_INSTALL_PREFIX=/usr/local \
	-DCMAKE_BUILD_TYPE=Release \
	-DENABLE_TESTING=0 \
	-GNinja && \
	cmake --build build && \
	cmake --install build && \
	cd /usr/src/ && \
	git clone --recursive https://github.com/libspatialindex/libspatialindex && \
	cd libspatialindex && \
	cmake -B build \
	-DCMAKE_INSTALL_PREFIX=/usr/local && \
	cmake --build build && \
	cmake --install build && \
	cd /usr/src/ && \
	git clone --recursive https://luajit.org/git/luajit.git -b ${LUAJIT_VERSION} && \
	cd luajit && \
	make amalg && make install && \
	cd /usr/src/

FROM dev as builder
LABEL build_version="iri-research.org version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="I.R.I."

WORKDIR /usr/src/luanti

RUN	if [ -z ${LUANTI_RELEASE+x} ]; then \
	LUANTI_RELEASE=$(curl -sX GET "https://api.github.com/repos/luanti-org/luanti/releases/latest" \
	| awk '/tag_name/{print $4;exit}' FS='[""]'); \
	fi && if [ -z ${LUANTI_RELEASE+x} ]; then \
	LUANTI_RELEASE="${LUANTI_RELEASE_DEFAULT}"; \
	fi && \
	echo "LUANTI_RELEASE=${LUANTI_RELEASE}" && \
	curl -s -o \
	/tmp/luanti-src.tar.gz -L \
	"https://github.com/luanti-org/luanti/archive/${LUANTI_RELEASE}.tar.gz" && \
	tar xf \
	/tmp/luanti-src.tar.gz --strip-components=1

WORKDIR /usr/src/luanti
RUN cmake -B build \
	-DCMAKE_INSTALL_PREFIX=/usr/local \
	-DCMAKE_BUILD_TYPE=Release \
	-DBUILD_SERVER=TRUE \
	-DENABLE_PROMETHEUS=TRUE \
	-DBUILD_UNITTESTS=FALSE -DBUILD_BENCHMARKS=FALSE \
	-DBUILD_CLIENT=FALSE \
	-GNinja && \
	cmake --build build && \
	cmake --install build && \
	curl -s -o \
	/tmp/minetest_game.tar.gz -L \
	"https://github.com/luanti-org/minetest_game/archive/${LUANTI_RELEASE}.tar.gz"

RUN mkdir -p /usr/local/share/luanti/games/minetest_game && \
	if [ -z ${MINETEST_GAME_RELEASE+x} ]; then \
	MINETEST_GAME_RELEASE=$(curl -sX GET "https://api.github.com/repos/luanti-org/luanti/releases/latest" \
	| awk '/tag_name/{print $4;exit}' FS='[""]'); \
	fi && if [ -z ${MINETEST_GAME_RELEASE+x} ]; then \
	MINETEST_GAME_RELEASE="${MINETEST_GAME_RELEASE_DEFAULT}"; \
	fi && \
	echo "MINETEST_GAME_RELEASE=${MINETEST_GAME_RELEASE}" && \
	curl -s -o \
	/tmp/minetest_game.tar.gz -L \
	"https://github.com/luanti-org/luanti/archive/${MINETEST_GAME_RELEASE}.tar.gz" && \
	tar xf /tmp/minetest_game.tar.gz -C /usr/local/share/luanti/games/minetest_game --strip-components=1 && \
	mkdir -p /usr/local/share/luanti/games/mtl

COPY game.conf settingtypes.txt /usr/local/share/luanti/games/mtl/
COPY mods /usr/local/share/luanti/games/mtl/mods
COPY utils /usr/local/share/luanti/games/mtl/utils

WORKDIR /usr/local/share/luanti/games/mtl/utils

RUN chmod +x ./optimize_textures.sh && ./optimize_textures.sh


FROM $DOCKER_IMAGE AS runtime
ARG PUID
ARG GUID
ARG BUILD_DATE
ARG VERSION

LABEL build_version="iri-research.org version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="I.R.I."

RUN apk add --no-cache curl gmp libstdc++ libgcc libpq jsoncpp zstd-libs \
	sqlite-libs postgresql hiredis leveldb bash inotify-tools && \
	echo "GUID=${GUID}" && \
	addgroup -g ${GUID} minetest && \
	adduser -D -G minetest minetest --uid ${PUID} -h /var/lib/minetest && \
	mkdir /var/log/minetest && \
	chown -R minetest:minetest /var/lib/minetest /var/log/minetest

WORKDIR /var/lib/minetest

COPY --from=builder /usr/local/share/luanti /usr/local/share/luanti
COPY --from=builder /usr/local/bin/luantiserver /usr/local/bin/luantiserver
COPY --from=builder /usr/local/share/doc/luanti /usr/local/share/doc/luanti 
COPY --from=builder /usr/local/share/doc/luanti/minetest.conf.example /etc/minetest/minetest.conf
COPY --from=builder /usr/local/lib/libspatialindex* /usr/local/lib/
COPY --from=builder /usr/local/lib/libluajit* /usr/local/lib/


USER minetest:minetest

EXPOSE 30000/udp 30000/tcp
VOLUME /var/lib/minetest/ /etc/minetest/ /var/log/minetest/

ENTRYPOINT ["/usr/local/bin/luantiserver"]
CMD ["--config", "/etc/minetest/minetest.conf", "--logfile", "/var/log/minetest/debug.txt", "--world", "/var/lib/minetest/worlds/world", "--gameid", "mtl", "--port", "30000"]
